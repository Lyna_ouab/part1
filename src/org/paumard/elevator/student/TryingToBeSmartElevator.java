package org.paumard.elevator.student;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.paumard.elevator.Elevator;
import org.paumard.elevator.model.Person;

public class TryingToBeSmartElevator implements Elevator {
	
	
    private int elevatorCapacity;
	private int currentFloor = 1;
	private List<List<Person>> peopleByFloor;
	private boolean lastPersonArrived = false;

	private List<Person> peopleInElevator = new ArrayList<>();
	private int numberOfPeopleWaitingAtFloors = 0;

    public TryingToBeSmartElevator(int elevatorCapacity) {
        this.elevatorCapacity = elevatorCapacity;
    }

    @Override
    public void startsAtFloor(LocalTime time, int initialFloor) {
    	this.currentFloor = initialFloor;    	
    }

    @Override
    public void peopleWaiting(List<List<Person>> peopleByFloor) {
		this.peopleByFloor = peopleByFloor;
		this.numberOfPeopleWaitingAtFloors = 
				peopleByFloor.stream()
							.mapToInt(List::size)
							.sum();
    }

    @Override
    public int chooseNextFloor() {
    	this.peopleWaiting(peopleByFloor);
    	if  (this.numberOfPeopleWaitingAtFloors == 0 && this.peopleInElevator.size() == 0){ //personne dans les �tages 
	    		if (lastPersonArrived) { //fini
	    			return 1;  
	    		}else { // en attente
	    			return currentFloor;
	    		}
	    		
    	}else { // des personne qui attendent dans les �tages et/ou des personnes dans elevator 
    		if (this.peopleInElevator.size() == 0) {
    			if (!this.peopleByFloor.get(this.currentFloor - 1).isEmpty()&& this.peopleByFloor.get(this.currentFloor - 1).size() < this.elevatorCapacity){
		        return this.peopleByFloor.get(this.currentFloor - 1).stream().mapToInt(p -> p.getDestinationFloor()).min().orElseThrow();
    			}
    			return findFloorWithPeople();	

    		}else {
    			int floor = (int)peopleInElevator.stream().mapToInt(p -> p.getDestinationFloor()).distinct().count();
    			if (floor == 1) {		
    				return this.peopleInElevator.get(0).getDestinationFloor();
    			}else {
    				return peopleInElevator.stream().mapToInt(p -> p.getDestinationFloor()).min().orElseThrow();
    			}    			
    		}
    	} 
    }
    private int findFloorWithPeople() {
        int floorWithPeople = 0;
        for (int floorIndex = 0 ; floorIndex < this.peopleByFloor.size() ; floorIndex++) {
            int numberOFPeopleWaiting = this.peopleByFloor.get(floorIndex).size();
            if (numberOFPeopleWaiting != 0 && this.peopleByFloor.get(floorIndex) != null) {
                return floorWithPeople = floorIndex + 1;
            }
        }
    	System.out.println("****");
        return floorWithPeople + 1;
    }
    
    @Override
    public void arriveAtFloor(int floor) {
    	this.currentFloor = floor;
    	
    }

    @Override
    public void loadPerson(Person person) {
//        if (this.peopleInElevator.size() < this.elevatorCapacity) {
	    	this.peopleInElevator.add(person);
	    	this.numberOfPeopleWaitingAtFloors--;
	        this.peopleByFloor.get(this.currentFloor - 1).remove(person);
//        }
        
    }

    @Override
    public void unloadPerson(Person person) {
     	this.peopleInElevator.remove(person);
    }

    @Override
    public void newPersonWaitingAtFloor(int floor, Person person) {
    	this.numberOfPeopleWaitingAtFloors++;
        this.peopleByFloor.get(floor - 1).add(person);
    }

    @Override
    public void lastPersonArrived() {
    	this.lastPersonArrived  = true;
    }
}
